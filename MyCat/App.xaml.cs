﻿//AppCenter
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
//Forms
using Xamarin.Forms;

namespace MyCat
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            AppCenter.Start("ios=0ef2d24c-c65e-4a86-940c-53f1185bec10;" +
                  "uwp={Your UWP App secret here};" +
                  "android={Your Android App secret here}",
                  typeof(Analytics), typeof(Crashes));

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
